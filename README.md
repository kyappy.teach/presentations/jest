# Jest

## Table Of Contents

- [About the project](#about-the-project)
  - [Summary](#summary)
  - [Build With](#build-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
  - [Build](#build)
  - [Test](#test)
- [Contributing](#contributing)
  - [Process](#process)
  - [Team](#team)
- [Contact](#contact)
- [License](#license)

## About the project

### Summary

This Typescript project is meant to learn how to create unit tests with Jest.

### Build With

- [TypeScript](https://www.typescriptlang.org/) is an open-source language which builds on JavaScript
- [Webpack](https://webpack.js.org/) is a static module bundler for modern JavaScript applications
- [Jest](https://jestjs.io/) is a JavaScript Testing Framework

## Getting started

### Prerequisites

- [Node.js](https://nodejs.org/)
- [yarn](https://yarnpkg.com/) or [npm](https://www.npmjs.com/)

### Installation

1. Clone the repo

```bash
git clone git@gitlab.com:kyappy.teach/presentations/jest.git
```

2. Install NPM packages

```bash
yarn install
```
or
```bash
npm install
```

## Usage

### Build

Run `yarn build` or `npm build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Test

Run `yarn test` or `npm test` to execute the unit tests via [Jest](https://jestjs.io/). Use the `--watchAll` flag for a long-running test session.

## Contributing

### Process

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. [Fork the Project][fork-url]
2. Create your Feature Branch (`git checkout -b feature/amazing-feature`)
3. Commit your Changes (`git commit -m "add some amazing feature"`)
4. Push to the Branch (`git push origin feature/amazing-feature`)
5. Open a Pull Request

### Team

- [Marc Gavanier][linkedin-url]
- [Add your name here...](#contributing)

## Contact

You can reach me at [marc.gavanier@gmail.com](marc.gavanier@gmail.com).

## License

Released under the terms of the `MIT` license. See [LICENSE](/LICENSE) for the full details.

[fork-url]: https://gitlab.com/kyappy.teach/presentations/jest/-/forks/new
[linkedin-url]: https://www.linkedin.com/in/marc-gavanier

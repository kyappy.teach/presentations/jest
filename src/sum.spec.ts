import {sum} from './sum';

describe('sum', (): void => {
    it('should compute the sum of two numbers', (): void => {
        expect(sum(1, 2)).toBe(3);
    });
})
